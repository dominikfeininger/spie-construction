sap.ui.define([
  "spie/hr/construction/controller/BaseController"
], function (Controller) {
  "use strict";

  return Controller.extend("spie.hr.construction.controller.NotFound", {

    onInit: function () {
      this.getRouter().getTarget("TargetNotFound").attachDisplay(this._onNotFoundDisplayed, this);
    },

    _onNotFoundDisplayed: function () {
      this.getModel("appView").setProperty("/layout", "OneColumn");
    }
  });
});
