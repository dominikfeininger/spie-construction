sap.ui.define([
  "spie/hr/construction/controller/BaseController",
  "sap/ui/model/json/JSONModel",
  "../model/formatter",
  "sap/m/library"
], function (BaseController, JSONModel, formatter, mobileLibrary) {
  "use strict";

  return BaseController.extend("spie.hr.construction.controller.Detail", {

    onInit: function () {

      this.getRouter().getRoute("Detail").attachPatternMatched(this._onObjectMatched, this);

      this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
    },

    /**
 * Binds the view to the object path and expands the aggregated line items.
 * @function
 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
 * @private
 */
    _onObjectMatched: function (oEvent) {
      var sProductd = oEvent.getParameter("arguments").productId;
      if (sProductd) {
        this.getModel("appView").setProperty("/detailsTabbarSelectedKey", "allocation")
      } else {
        this.getModel("appView").setProperty("/detailsTabbarSelectedKey", "construction")
      }

    },

    _onMetadataLoaded: function () {

    },
  });
});
