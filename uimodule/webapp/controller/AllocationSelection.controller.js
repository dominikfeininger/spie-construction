sap.ui.define([
  "spie/hr/construction/controller/BaseController"
], function(Controller) {
  "use strict";

  return Controller.extend("spie.hr.construction.controller.AllocationSelection", {});
});
