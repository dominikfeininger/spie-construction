sap.ui.define([
  "sap/ui/test/Opa5",
  "spie/hr/construction/test/integration/arrangements/Startup",
  "spie/hr/construction/test/integration/BasicJourney"
], function(Opa5, Startup) {
  "use strict";

  Opa5.extendConfig({
    arrangements: new Startup(),
    pollingInterval: 1
  });

});
